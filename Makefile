include /usr/share/dpkg/default.mk

PACKAGE := libjs-framework7
include defines.mk

all: $(DEB)

DEB=$(PACKAGE)_$(DEB_VERSION_UPSTREAM_REVISION)_all.deb
DSC=$(PACKAGE)_$(DEB_VERSION_UPSTREAM_REVISION).dsc
ORIG_SRC_TAR=$(PACKAGE)_$(DEB_VERSION_UPSTREAM).orig.tar.gz

BUILDDIR=$(PACKAGE)-$(DEB_VERSION_UPSTREAM_REVISION)


$(BUILDDIR): debian
	rm -rf $@ $@.tmp
	mkdir $@.tmp
	cp -a $(SRCDIRS) $@.tmp
	cp -a debian defines.mk Makefile $@.tmp
	mv $@.tmp $@

.PHONY: dsc
dsc:
	$(MAKE) clean
	$(MAKE) $(DSC)
	lintian $(DSC)

$(ORIG_SRC_TAR): $(BUILDDIR)
	tar czf $(ORIG_SRC_TAR) --exclude="$(BUILDDIR)/debian" $(BUILDDIR)

$(DSC): $(ORIG_SRC_TAR) $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -S -us -uc

.PHONY: sbuild
sbuild: $(DSC)
	sbuild $(DSC)

.PHONY: deb
deb: $(DEB)

$(DEB): $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -b -us -uc
	lintian $(DEB)


install:
	install -d $(CSSDIR)
	install -d $(JSDIR)
	install -d $(FONTDIR)
	make -C $(F7DIR) install
	make -C $(F7ICONSDIR) install
	make -C $(MATERIALDIR) install

.PHONY: framework7
framework7:
	wget $(F7URL) -O framework7.tar.gz.tmp
	mv framework7.tar.gz.tmp framework7.tar.gz
	mkdir -p $(F7DIR).tmp
	tar -xf framework7.tar.gz -C $(F7DIR).tmp
	cp -ar $(F7DIR).tmp/js $(F7DIR)/
	cp -ar $(F7DIR).tmp/css $(F7DIR)/
	rm framework7.tar.gz
	rm -rf $(F7DIR).tmp

.PHONY: framework7-icons
framework7-icons:
	wget $(F7ICONSURL) -O f7icons.tar.gz.tmp
	mv f7icons.tar.gz.tmp f7icons.tar.gz
	tar -xf f7icons.tar.gz
	cp -rf $(F7ICONSDIR)-$(F7ICONSVER)/css $(F7ICONSDIR)/
	cp -rf $(F7ICONSDIR)-$(F7ICONSVER)/fonts $(F7ICONSDIR)/
	rm -rf $(F7ICONSDIR)-$(F7ICONSVER)
	rm f7icons.tar.gz

.PHONY: material-icons
material-icons:
	mkdir -p $(MATERIALDIR)
	for i in $(MATERIALFONTS) $(MATERIALCSS); do \
	    wget $(MATERIALURL)/$$i -O material-icons/$$i.tmp; \
	    mv material-icons/$$i.tmp material-icons/$$i; \
	done

.PHONY: download
download: framework7 framework7-icons material-icons

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(DEB)
	tar cf - $(DEB) | ssh -X repoman@repo.proxmox.com -- upload --product pmg --dist $(UPLOAD_DIST)


.PHONY: clean
clean:
	rm -rf $(PACKAGE)-[0-9]*/ build/
	rm -f $(PACKAGE)*.tar* *.deb *.dsc *.build *.buildinfo *.changes
