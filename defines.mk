PREFIX := /usr
DATAROOTDIR := $(PREFIX)/share
TARGETDIR := $(DATAROOTDIR)/javascript/framework7
CSSDIR := $(DESTDIR)/$(TARGETDIR)/css
JSDIR := $(DESTDIR)/$(TARGETDIR)/js
FONTDIR := $(DESTDIR)/$(TARGETDIR)/fonts

F7DIR := framework7
F7VER := 4.4.7
F7URL := https://github.com/framework7io/framework7/releases/download/v$(F7VER)/framework7.tar.gz

F7ICONSDIR := framework7-icons
F7ICONSVER := 2.3.1
F7ICONSURL := https://github.com/framework7io/framework7-icons/archive/v$(F7ICONSVER).tar.gz

MATERIALDIR := material-icons
MATERIALVER := 3.0.1
MATERIALURL := https://github.com/google/material-design-icons/raw/$(MATERIALVER)/iconfont

MATERIALFONTS := MaterialIcons-Regular.ttf	\
		 MaterialIcons-Regular.woff	\
		 MaterialIcons-Regular.woff2

MATERIALCSS := material-icons.css

SRCDIRS := $(F7DIR) $(F7ICONSDIR) $(MATERIALDIR)
